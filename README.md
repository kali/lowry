Set up a testing environment:
```
sudo apt install slapd
sudo slapadd -n 1 -l examples/data.ldif
```

Run it:
```
make all
./lowry -config examples/lowry.conf
```

Now go to your browser, open http://localhost:8080 and you can login as:
* Usuaria: user
* Contraseña: foobar

Or as admin:
* Usuaria: superuser
* Contraseña: foobar


# tests

The tests needs a clean ldap with the `examples/data.ldif` added.

You can run them:
```
make test
```
