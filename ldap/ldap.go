package ldap

import (
	"github.com/go-ldap/ldap"
)

// Ldap configuration
type Ldap struct {
	addr string
	dc   string
	pass string
}

// Init the Ldap
func Init(addr string, dc string, pass string) (*Ldap, error) {
	l := Ldap{addr, dc, pass}
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	return &l, nil
}

func (l Ldap) connect() (*ldap.Conn, error) {
	conn, err := ldap.Dial("tcp", l.addr)
	if err != nil {
		return nil, err
	}
	err = conn.Bind("cn=nss,"+l.dc, l.pass)
	return conn, err
}
