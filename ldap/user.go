package ldap

import (
	"errors"
	"fmt"

	"github.com/go-ldap/ldap"
)

// ValidateUser in the ldap
func (l *Ldap) ValidateUser(user string, pass string) error {
	conn, err := l.login(user, pass)
	if err == nil {
		conn.Close()
	}
	return err
}

// ChangePass changes logged in user's password
func (l *Ldap) ChangePass(user string, oldpass string, newpass string) error {
	conn, err := l.login(user, oldpass)
	if err != nil {
		return err
	}
	defer conn.Close()

	passwordModifyRequest := ldap.NewPasswordModifyRequest("", oldpass, newpass)
	_, err = conn.PasswordModify(passwordModifyRequest)
	return err
}

func (l *Ldap) login(user string, password string) (*ldap.Conn, error) {
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	entry, err := l.searchUser(user, conn)
	if err != nil {
		conn.Close()
		return nil, err
	}
	userdn := entry.DN
	return conn, conn.Bind(userdn, password)
}

func (l *Ldap) searchUser(user string, conn *ldap.Conn) (entry *ldap.Entry, err error) {
	searchRequest := ldap.NewSearchRequest(
		"ou=people,"+l.dc,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectClass=posixAccount)(uid=%s))", user),
		[]string{"dn"},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return entry, err
	}

	switch len(sr.Entries) {
	case 1:
		entry = sr.Entries[0]
		return entry, nil
	case 0:
		return entry, errors.New("No user found")
	default:
		return entry, errors.New("More than one user found!!!")
	}
}
