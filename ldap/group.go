package ldap

import (
	"fmt"

	"github.com/go-ldap/ldap"
)

// InGroup checks if user is part of group
func (l Ldap) InGroup(user string, group string) bool {
	conn, err := l.connect()
	if err != nil {
		return false
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		fmt.Sprintf("cn=%s,ou=group,%s", group, l.dc),
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectClass=posixGroup)(memberUid=%s))", user),
		[]string{"dn"},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	return err == nil && len(sr.Entries) > 0
}
