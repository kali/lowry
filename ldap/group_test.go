package ldap

import "testing"

const (
	admin = "superuser"
	group = "adm"
)

func TestInGroup(t *testing.T) {
	l, err := Init(addr, dc, pass)
	if err != nil {
		t.Errorf("Error on Init(): %v", err)
	}

	if !l.InGroup(admin, group) {
		t.Errorf("%s should be part of group %s", admin, group)
	}

	if l.InGroup(user, group) {
		t.Errorf("%s should not be part of group %s", user, group)
	}

	if l.InGroup(admin, "nonexistinggroup") {
		t.Errorf("%s should not be part of nonexistinggroup", admin)
	}
}
