package ldap

import "testing"

const (
	addr = "localhost:389"
	dc   = "dc=nodomain"
	pass = "foobar"
)

func TestInit(t *testing.T) {
	_, err := Init(addr, dc, pass)
	if err != nil {
		t.Errorf("Error on Init(): %v", err)
	}
}
