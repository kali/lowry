package ldap

import "testing"

const (
	user     = "user"
	userPass = "foobar"
)

func TestValidate(t *testing.T) {
	l, err := Init(addr, dc, pass)
	if err != nil {
		t.Errorf("Error on Init(): %v", err)
	}

	err = l.ValidateUser(user, userPass)
	if err != nil {
		t.Errorf("Error on ValidateUser(): %v", err)
	}
}

func TestValidateFails(t *testing.T) {
	l, err := Init(addr, dc, pass)
	if err != nil {
		t.Errorf("Error on Init(): %v", err)
	}

	err = l.ValidateUser(user, userPass+"bar")
	if err == nil {
		t.Errorf("ValidateUser() didn't fail to auth the user")
	}
}

func TestChangePass(t *testing.T) {
	l, err := Init(addr, dc, pass)
	if err != nil {
		t.Errorf("Error on Init(): %v", err)
	}

	err = l.ChangePass(user, userPass, "newpass")
	if err != nil {
		t.Errorf("Error on ChangePass(): %v", err)
	}

	err = l.ChangePass(user, "newpass", userPass)
	if err != nil {
		t.Errorf("Error on the second ChangePass(): %v", err)
	}
}
