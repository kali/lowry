package main

import (
	"log"

	"0xacab.org/sindominio/lowry/ldap"
	"0xacab.org/sindominio/lowry/server"
	"github.com/namsral/flag"
)

func main() {
	var (
		ldapaddr = flag.String("ldapaddr", "localhost:389", "LDAP server address and port")
		ldapdc   = flag.String("ldapdc", "", "LDAP domain components")
		nsspass  = flag.String("nsspass", "", "Password of the LDAP `nss' user")
		httpaddr = flag.String("httpaddr", ":8080", "Web server address and port")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/lowry.conf", "Path to configuration file")
	flag.Parse()

	l, err := ldap.Init(*ldapaddr, *ldapdc, *nsspass)
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(server.Serve(*httpaddr, l))
}
